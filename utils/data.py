import os

import pandas


def get_data():
    """Get taxonomy and TCGA data as single dataframe.
    :return: pandas dataframe with one row per patient and one column per gene.
             Indices:
             - dataset: 'Taxonomy (GSE103479)' or 'TCGA COAD-READ';
             - patient_id: unique identifier;
             - cms_class: 'CMS1', 'CMS2', 'CMS3' or 'CMS4'.
    """
    data = pandas.concat([
        pandas.read_csv(os.path.join('processed_data', 'taxonomy_cohort.csv'), index_col=[0, 1]),
        pandas.read_csv(os.path.join('processed_data', 'tcga_coad_read_cohort.csv'), index_col=[0, 1])],
                      keys=['Taxonomy (GSE103479)', 'TCGA COAD-READ'],
                      names=['dataset', 'patient_id', 'cms_class'],
                      axis=0)

    # Retain only patients with CMS1-4 labels (exclude both "NOLBL" and missing label)
    data.query('cms_class.isin(["CMS1", "CMS2", "CMS3", "CMS4"])', inplace=True)

    # Derived quantities
    data.loc[:, 'IAPs (BIRC2 + BIRC3)'] = data['BIRC2'] + data['BIRC3']

    return data
