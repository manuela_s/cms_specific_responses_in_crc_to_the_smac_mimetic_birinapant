import os

import matplotlib
import matplotlib.pyplot
import seaborn

import py_utils.outlier_scale
import py_utils.rounding

CMS_COLORS = ['#E89E33', '#0073AC', '#D079A4', '#009E76']
CMS_ORDER = ['CMS1', 'CMS2', 'CMS3', 'CMS4']


def _subplot_helper(data, color):
    """Make a boxplot with overlaid swarmplot.
    :param data: pandas dataframe, one row per patient and a column 'expression' with the gene expression values;
                 Indices should include 'cms_class' with values from CMS_ORDER;
    :param color: unused.
    """
    ax = matplotlib.pyplot.gca()

    # Check for outliers (data-points more extreme than 1.5 times the inter-quartile range)
    q1, q3 = data.expression.quantile([0.25, 0.75])
    upper = 1.5 * (q3 - q1) + q3
    highest_non_outlier = data.expression[data.expression < upper].max()
    threshold = py_utils.rounding.round_up_to_n_significant_digits(highest_non_outlier, 2)
    if data.expression.max() > threshold * 1.5:
        # Use logarithmic scale for outliers
        ax.axhline(threshold, color='k', linestyle=':')
        ax.set_yscale('symlog', linthreshy=threshold, linscaley=3)
        ax.yaxis.set_major_locator(py_utils.outlier_scale.OutlierLogLocator(linthresh=threshold, base=10))
        ax.yaxis.set_minor_locator(
            py_utils.outlier_scale.OutlierLogLocator(linthresh=threshold, base=10, subs=range(1, 9)))
        ax.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:g}'))

    # Make boxplot with overlaying swarmplot color-coded by 'cms_class'
    params = dict(x='cms_class',
                  y='expression',
                  order=CMS_ORDER,
                  hue='cms_class',
                  hue_order=CMS_ORDER,
                  palette=CMS_COLORS,
                  data=data)
    b = seaborn.boxplot(dodge=False,
                        width=0.9,
                        saturation=1,
                        showfliers=False,
                        **params)
    s = seaborn.swarmplot(size=3,
                          **params)

    # Set transparency on boxplot fill color
    for patch in b.artists:
        r, g, b, a = patch.get_facecolor()
        patch.set_facecolor((r, g, b, 0.3))

    # Modify quartile line objects to be more visible
    for l in s.findobj(matplotlib.lines.Line2D):
        if l.axes is None:
            continue
        l.set_zorder(3)


def figure_helper(data, genes, name):
    """Make a figure with datasets as rows and genes as columns.
    :param data: pandas dataframe, 1 row per patient and one column per gene, as returned by utils.data.get_data();
    :param genes: list, list of genes to plot;
    :param name: string, figure label.
    """
    # Subset to only include selected genes
    data = data[genes]

    data = data.stack().to_frame('expression')
    data.index.names = ['dataset', 'sample_id', 'cms_class', 'gene']

    g = seaborn.FacetGrid(
        col='gene',
        row='dataset',
        row_order=['Taxonomy (GSE103479)', 'TCGA COAD-READ'],
        sharey=False,
        data=data.reset_index())

    g.map_dataframe(_subplot_helper)

    g.set_titles("{col_name}")
    g.set_axis_labels('CMS subtype')

    g.axes[0, 0].set_ylabel('Taxonomy (GSE103479)\n(n={:.0f})\n\nNormalized expression [a.u.]'.format(
            len(data.query('dataset=="Taxonomy (GSE103479)"').index.get_level_values('sample_id').unique())))
    g.axes[1, 0].set_ylabel('TCGA COAD-READ\n(n={:.0f})\n\nNormalized expression [a.u.]'.format(
            len(data.query('dataset=="TCGA COAD-READ"').index.get_level_values('sample_id').unique())))

    matplotlib.pyplot.subplots_adjust(left=0.1, bottom=0.15, right=0.95, top=0.95, wspace=0.3, hspace=0.25)
    matplotlib.pyplot.tight_layout()

    matplotlib.pyplot.savefig(
        os.path.join('outputs', name + '.pdf'),
        bbox_inches="tight")

    print('Figure saved in "outputs/{}.pdf".'.format(name))
