import os

import numpy
import pandas
import statsmodels.formula.api
import statsmodels.stats.anova
import statsmodels.stats.multicomp

import py_utils.format_pvalue

CMS_COLORS = ['#E89E33', '#0073AC', '#D079A4', '#009E76']
CMS_ORDER = ['CMS1', 'CMS2', 'CMS3', 'CMS4']


def _anova_p_helper(df):
    """Compute ANOVA p-value for association between 'expression' and 'cms_class'.
    :param df: pandas dataframe, one row per observation with columns 'expression' and 'cms_class';
    :return: float, p-value.
    """
    formula = 'expression ~ cms_class'
    model = statsmodels.formula.api.ols(formula, df).fit()
    aov_t = statsmodels.stats.anova.anova_lm(model, typ=1)

    return aov_t.loc['cms_class', 'PR(>F)']


def _tukey_posthoc_tests_helper(df):
    """Compute pairwise group comparisons with Tukey PostHoc tests for 'expression' grouped by 'cms_class'.
    :param df: pandas dataframe, one row per observation with columns 'expression' and 'cms_class';
    :return: pandas dataframe, one row per pair of 'cms_class' groups and columns: 'group1', 'group2', 'meandiff',
             'p-adj', 'lower', 'upper', 'reject'.
    """
    mc = statsmodels.stats.multicomp.pairwise_tukeyhsd(df.expression, df.cms_class)

    return pandas.DataFrame(mc.summary().data[1:], columns=mc.summary().data[0])


def stats(df, genes_order):
    """Compute statistics for association between gene expression and CMS class stratified by dataset.
    :param df: pandas dataframe, as returned by util.data.get_data();
    :param genes_order: list, gene names in desired order;

    Export results to excel spreadsheet.
    """

    # Convert to tall table with gene index
    df = df.stack().to_frame('expression')
    df.index.names = ['dataset', 'sample_id', 'cms_class', 'gene']
    df = df.reset_index()
    df.dataset = pandas.Categorical(df.dataset, ['Taxonomy (GSE103479)', 'TCGA COAD-READ'])
    df.gene = pandas.Categorical(df.gene, genes_order)
    df.sort_values(by=['dataset', 'sample_id', 'cms_class', 'gene'], inplace=True)

    # Compute statistics
    anova = df.groupby(['dataset', 'gene']).apply(_anova_p_helper).to_frame('overall_anova_pvalue')
    posthoc = df.groupby(['dataset', 'gene']).apply(_tukey_posthoc_tests_helper)
    stats = pandas.concat([anova, posthoc], axis=0, sort=False).sort_index()
    stats = stats[['group1', 'group2', 'meandiff', 'lower', 'upper', 'p-adj', 'overall_anova_pvalue']]

    # Format table for export
    stats.rename(columns={
        'group1': 'Group 1',
        'group2': 'Group 2',
        'meandiff': 'Mean difference',
        'lower': '2.5% CI',
        'upper': '97.5% CI',
        'p-adj': 'PostHoc Tukey P-value',
        'overall_anova_pvalue': 'Overall ANOVA P-value'
    }, inplace=True)

    stats['Mean difference'] = stats['Mean difference'].round(1)
    stats['2.5% CI'] = stats['2.5% CI'].round(1)
    stats['97.5% CI'] = stats['97.5% CI'].round(1)
    stats['PostHoc Tukey P-value'] = stats['PostHoc Tukey P-value'].apply(
        lambda x: '' if numpy.isnan(x) else py_utils.format_pvalue.format_pvalue(x))
    stats['Overall ANOVA P-value'] = stats['Overall ANOVA P-value'].apply(
        lambda x: '' if numpy.isnan(x) else py_utils.format_pvalue.format_pvalue(x))

    stats = stats.swaplevel('gene', 'dataset').sort_index().reset_index()
    stats.dataset = stats.dataset.astype('object')
    stats.gene = stats.gene.astype('object')
    stats.loc[stats.gene == stats.gene.shift(1), 'gene'] = ''
    stats.loc[stats.dataset == stats.dataset.shift(1), 'dataset'] = ''
    stats.rename(columns={
        'gene': 'Gene',
        'dataset': 'Dataset'},
        inplace=True)

    def color_by_cms(data):
        if data == 'CMS1':
            color = CMS_COLORS[0]
        elif data == 'CMS2':
            color = CMS_COLORS[1]
        elif data == 'CMS3':
            color = CMS_COLORS[2]
        elif data == 'CMS4':
            color = CMS_COLORS[3]
        else:
            return ''

        return 'background-color: {}'.format(color)

    # Apply style and export to excel spreadsheet
    stats.style. \
        applymap(color_by_cms, subset=['Group 1', 'Group 2']). \
        apply(lambda x: ['border-top-style: solid' if x['Overall ANOVA P-value'] else ''] * len(x), axis=1). \
        apply(lambda x: ['font-weight: bold' if x['Overall ANOVA P-value'] else ''] * len(x), axis=1). \
        set_properties(**{'text-align': 'left'}). \
        set_properties(subset=['Mean difference', '2.5% CI', '97.5% CI'], **{'text-align': 'right'}). \
        to_excel(os.path.join('outputs', 'supplementary_table_2.xlsx'),
                 index=False)

    print('Results from statistical analysis saved in outputs/supplementary_table_2.xlsx')
