import unittest

import py_utils.rounding


class RoundUpToNSignificantDigitsTestCase(unittest.TestCase):
    def test_small_positive_numbers1(self):
        self.assertEqual(0.2, py_utils.rounding.round_up_to_n_significant_digits(0.123, 1))

    def test_small_positive_numbers2(self):
        self.assertEqual(0.13, py_utils.rounding.round_up_to_n_significant_digits(0.123, 2))

    def test_small_positive_numbers3(self):
        self.assertEqual(0.93, py_utils.rounding.round_up_to_n_significant_digits(0.923, 2))

    def test_small_negative_numbers1(self):
        self.assertEqual(-0.1, py_utils.rounding.round_up_to_n_significant_digits(-0.123, 1))

    def test_small_negative_numbers2(self):
        self.assertEqual(-0.12, py_utils.rounding.round_up_to_n_significant_digits(-0.123, 2))

    def test_small_negative_numbers3(self):
        self.assertEqual(-0.92, py_utils.rounding.round_up_to_n_significant_digits(-0.923, 2))

    def test_big_positive_numbers1(self):
        self.assertEqual(200, py_utils.rounding.round_up_to_n_significant_digits(123, 1))

    def test_big_positive_numbers2(self):
        self.assertEqual(130, py_utils.rounding.round_up_to_n_significant_digits(123, 2))

    def test_big_positive_numbers3(self):
        self.assertEqual(930, py_utils.rounding.round_up_to_n_significant_digits(923, 2))

    def test_big_negative_numbers1(self):
        self.assertEqual(-100, py_utils.rounding.round_up_to_n_significant_digits(-123, 1))

    def test_big_negative_numbers2(self):
        self.assertEqual(-120, py_utils.rounding.round_up_to_n_significant_digits(-123, 2))

    def test_big_negative_numbers3(self):
        self.assertEqual(-920, py_utils.rounding.round_up_to_n_significant_digits(-923, 2))


if __name__ == '__main__':
    unittest.main()

