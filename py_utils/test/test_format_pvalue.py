import unittest

import py_utils.format_pvalue


class FormatPValueTestCase(unittest.TestCase):
    def test1(self):
        self.assertEqual('<0.0001', py_utils.format_pvalue.format_pvalue(0.0000001))

    def test2(self):
        self.assertEqual('0.28', py_utils.format_pvalue.format_pvalue(0.27890))

    def test3(self):
        self.assertEqual('0.01', py_utils.format_pvalue.format_pvalue(0.01234))

    def test4(self):
        self.assertEqual('0.002', py_utils.format_pvalue.format_pvalue(0.00223))

    def test5(self):
        self.assertEqual('0.0001', py_utils.format_pvalue.format_pvalue(0.00011))

    def test6(self):
        self.assertEqual('0.0500', py_utils.format_pvalue.format_pvalue(0.04999))

    def test7(self):
        self.assertEqual('0.051', py_utils.format_pvalue.format_pvalue(0.05123))

    def test8(self):
        self.assertEqual('0.0501', py_utils.format_pvalue.format_pvalue(0.0501))

    def test9(self):
        self.assertEqual('0.0500', py_utils.format_pvalue.format_pvalue(0.0500))

    def test_prefix1(self):
        self.assertEqual('p<0.0001', py_utils.format_pvalue.format_pvalue(0.0000001, 'p'))

    def test_prefix2(self):
        self.assertEqual('p=0.28', py_utils.format_pvalue.format_pvalue(0.27890, 'p'))

    def test_prefix3(self):
        self.assertEqual('p=0.01', py_utils.format_pvalue.format_pvalue(0.01234, 'p'))

    def test_prefix4(self):
        self.assertEqual('p_{a}=0.002', py_utils.format_pvalue.format_pvalue(0.00223, 'p_{a}'))

    def test_prefix5(self):
        self.assertEqual('p_{a}=0.0001', py_utils.format_pvalue.format_pvalue(0.00011, 'p_{a}'))


if __name__ == '__main__':
    unittest.main()
