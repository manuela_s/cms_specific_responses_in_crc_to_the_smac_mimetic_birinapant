import unittest

import numpy

import py_utils.outlier_scale


class OutlierLogLocatorTestCase(unittest.TestCase):

    def test_linear_and_log_range(self):
        locator = py_utils.outlier_scale.OutlierLogLocator(linthresh=1100, base=10)
        numpy.testing.assert_array_equal(locator.tick_values(0, 100000),
                                         [0, 200, 400, 600, 800, 1000, 10000, 100000])

    def test_linear_only_range(self):
        locator = py_utils.outlier_scale.OutlierLogLocator(linthresh=1100, base=10)
        numpy.testing.assert_array_equal(locator.tick_values(0, 22),
                                         [0, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20, 22.5])


if __name__ == '__main__':
    unittest.main()
