import math


def round_up_to_n_significant_digits(number, significant_digits):
    """Round a number up to n significant digits.
    :param number: numeric, number to round;
    :param significant_digits: integer, number of significant digits to round to;
    :return: float, rounded number.
    """
    digits = significant_digits - math.ceil(math.log10(abs(number)))
    return math.ceil(number*10**digits)/10**digits
