def _format_pvalue_helper(pvalue_prefix, value, sign='='):
    """Combine pvalue_prefix, sign and value."""
    if sign == '=' and not pvalue_prefix:
        return value
    else:
        return '{}{}{}'.format(pvalue_prefix, sign, value)


def format_pvalue(pvalue, pvalue_prefix=''):
    """Format pvalue with appropriate number of digits.
    :param pvalue: numeric, pvalue;
    :param pvalue_prefix: string, prefix to add to the p-value string;
    :return: pvalue_string: pvalue formatted as string.
    """

    if pvalue < 0.0001:
        return _format_pvalue_helper(pvalue_prefix, '0.0001', '<')
    elif pvalue < 0.001:
        return _format_pvalue_helper(pvalue_prefix, '{:.4f}'.format(pvalue))
    elif pvalue < 0.01:
        return _format_pvalue_helper(pvalue_prefix, '{:.3f}'.format(pvalue))
    elif '={:.3f}'.format(pvalue) == '=0.050':
        return _format_pvalue_helper(pvalue_prefix, '{:.4f}'.format(pvalue))
    elif '={:.2f}'.format(pvalue) == '=0.05':
        return _format_pvalue_helper(pvalue_prefix, '{:.3f}'.format(pvalue))
    else:
        return _format_pvalue_helper(pvalue_prefix, '{:.2f}'.format(pvalue))
