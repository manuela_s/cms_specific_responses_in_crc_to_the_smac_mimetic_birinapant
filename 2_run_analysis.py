#!/usr/bin/env python3

import os
import utils.data
import utils.figures
import utils.stats


MAIN_GENES = ['TNF', 'IAPs (BIRC2 + BIRC3)', 'XIAP', 'DIABLO']
SUPPLEMENTARY_GENES = ['BIRC2', 'BIRC3']


if __name__ == '__main__':
    os.makedirs('outputs', exist_ok=True)

    data = utils.data.get_data()

    # Figure 5
    utils.figures.figure_helper(data, MAIN_GENES, 'figure_5')

    # Supplementary Figure 2
    utils.figures.figure_helper(data, SUPPLEMENTARY_GENES, 'supplementary_figure_2')

    # Supplementary Table 2
    utils.stats.stats(data, MAIN_GENES + SUPPLEMENTARY_GENES)

    print('Analysis completed successfully')
