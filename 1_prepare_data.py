#!/usr/bin/env python3

import os
import pandas


GENES = ['TNF', 'XIAP', 'DIABLO', 'BIRC2', 'BIRC3']


def prepare_taxonomy_cohort():
    """Combine clinical, subtyping and transcriptomics data for the taxonomy in-house cohort (Allen et al., JCO Precis
    Oncol., 2018; PMID: 30088816)."""
    data = pandas.read_csv(os.path.join('data', 'clinical_taxonomy_cohort.csv'), index_col=['patient_id']).join(
        pandas.read_csv(os.path.join('data', 'transcriptomics_taxonomy_cohort.csv')).
        groupby(['patient_id', 'gene_symbol']).expression.mean().unstack('gene_symbol')).\
        set_index('cms_class', append=True)

    # Re-order columns
    data = data[GENES]

    # Export data for downstream analysis
    data.to_csv(os.path.join('processed_data', 'taxonomy_cohort.csv'))


def _get_tcga_pancancer_clinical():
    """"Read in TCGA Pancancer curated clinical data (Liu et al., Cell, 2018; PMID: 29625055)."""
    t = pandas.read_excel(os.path.join('download', 'TCGA-CDR-SupplementalTableS1.xlsx'),
                          sheet_name='TCGA-CDR',
                          index_col=0,
                          na_values=['[Not Available]', '[Unknown]', '[Discrepancy]', '[Not Evaluated]',
                                     '[Not Applicable]'],
                          )
    t.rename(columns={
        'bcr_patient_barcode': 'patient_id',
        'type': 'cancer_type'},
        inplace=True)

    # Include only non-redacted patients
    t = t.query('Redaction!="Redacted"')

    return t[['patient_id', 'cancer_type']].set_index('patient_id')


def _get_tcga_pancancer_transcriptomics():
    """"Read in TCGA Pancancer curated level 4 transcriptomics data
    (https://gdc.cancer.gov/about-data/publications/pancanatlas)."""

    filename = os.path.join('download', 'EBPlusPlusAdjustPANCAN_IlluminaHiSeq_RNASeqV2.geneExp.tsv')

    # Get all gene_ids to determine which rows can be skipped
    gene_ids = pandas.read_csv(filename, usecols=['gene_id'], sep='\t')

    t = pandas.read_csv(
        filename,
        index_col=0,
        skiprows=gene_ids.query("not gene_id.str.replace('\\|[0-9]+', '').isin(@GENES)").index + 1,
        sep='\t')

    # Transpose to have samples in rows and genes in columns
    t = t.T

    # Add identifier as per standard TCGA nomenclature (https://docs.gdc.cancer.gov/Encyclopedia/pages/TCGA_Barcode/)
    t.index.rename('id', inplace=True)
    t['patient_id'] = t.index.get_level_values('id').str[0:12]
    t['sample_type'] = t.index.get_level_values('id').str[13:15]

    # Aggregate values by median for patients with multiple measurements from the same 'sample_type'.
    # Patients affected include: 'TCGA-06-0211', 'TCGA-FG-5965', 'TCGA-TQ-A7RK', 'TCGA-DU-6404', 'TCGA-06-0156',
    # 'TCGA-DU-6407', 'TCGA-23-1023', 'TCGA-21-1076', 'TCGA-DD-AACA', 'TCGA-BF-A3DL'
    t = t.groupby(['patient_id', 'sample_type']).median()

    # Include only patients with measurements from primary tumour samples (See also
    # https://gdc.cancer.gov/resources-tcga-users/tcga-code-tables/sample-type-codes)
    t = t.query('sample_type=="01"').reset_index('sample_type', drop=True)

    # Clean-up gene names to match those in in-house taxonomy cohort
    t.columns = t.columns.str.replace('\\|[0-9]+', '')

    return t


def _get_tcga_coad_read_consensus_molecular_subtyping():
    """"Read in TCGA COAD-READ Consensus Molecular Subtypes (CMS) labels (Guinney et al., Nat. Med., 2015;
     PMID: 26457759; https://www.ncbi.nlm.nih.gov/pubmed/26457759; synapse ids: syn2623706 and syn4978511)."""
    t = pandas.read_csv(os.path.join('download', 'cms_labels_public_all.txt'), delimiter='\t')

    # Include only patients from the TCGA collection
    t.query('dataset=="tcga"', inplace=True)
    t.rename(columns={
        'sample': 'patient_id',
        'CMS_final_network_plus_RFclassifier_in_nonconsensus_samples': 'cms_class'},
        inplace=True)

    return t[['patient_id', 'cms_class']].set_index('patient_id')


def prepare_tcga_coad_read_cohort():
    """Combine clinical, subtyping and transcriptomics data for TCGA patients with colon (COAD) or rectal (READ) cancer.
    """
    data = _get_tcga_pancancer_clinical().query('cancer_type.isin(["COAD", "READ"])').drop(columns=['cancer_type']). \
        join(_get_tcga_pancancer_transcriptomics()[GENES], how='inner'). \
        join(_get_tcga_coad_read_consensus_molecular_subtyping()). \
        set_index(['cms_class'], append=True)

    # Export data for downstream analysis
    data.to_csv(os.path.join('processed_data', 'tcga_coad_read_cohort.csv'))


if __name__ == '__main__':
    os.makedirs('processed_data', exist_ok=True)

    prepare_taxonomy_cohort()
    prepare_tcga_coad_read_cohort()

    print('Data prepared successfully')
