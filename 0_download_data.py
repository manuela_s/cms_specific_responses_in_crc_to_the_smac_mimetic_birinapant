#!/usr/bin/env python3

import getpass
import os
import subprocess
import sys
import urllib.request

import synapseclient
import synapseutils


def synapse_login():
    """Ask user for synapse login details.
    :return: synapse object.
    """
    while True:
        email_address = input('Synapse email address: ')
        password = getpass.getpass('Synapse password (hidden): ')
        try:
            syn = synapseclient.login(email=email_address, password=password)
        except synapseclient.exceptions.SynapseAuthenticationError as e:
            print(e)
            continue
        else:
            break
    return syn


def download_tcga_data(syn):
    """Download TCGA datasets to 'download' directory.
    :param syn: logged-in synapse object;
    """
    print('Downloading CMS subtyping data from synapse entity syn4978511: cms_labels_public_all.txt')
    synapseutils.syncFromSynapse(syn, entity='syn4978511', path='download')  # Full data for CMS paper at syn2623706

    print('Downloading clinical data from TCGA PanCanAtlas: TCGA-CDR-SupplementalTableS1.xlsx')
    urllib.request.urlretrieve(
        'https://api.gdc.cancer.gov/data/1b5f413e-a8d1-4d10-92eb-7c4ae739ed81',
        os.path.join('download', 'TCGA-CDR-SupplementalTableS1.xlsx'))

    print('Downloading transcriptomics data from TCGA PanCanAtlas: EBPlusPlusAdjustPANCAN_IlluminaHiSeq_RNASeqV2.geneExp.tsv')
    urllib.request.urlretrieve(
        'http://api.gdc.cancer.gov/data/3586c0da-64d0-4b74-a449-5ff4d9136611',
        os.path.join('download', 'EBPlusPlusAdjustPANCAN_IlluminaHiSeq_RNASeqV2.geneExp.tsv'))


def check_download_checksums():
    """Check that checksums of downloaded files match expectations."""
    # Capture output for use in jupyter notebook
    completed_process = subprocess.run(['md5sum', '-c', 'tcga_download.md5'],
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT)
    if completed_process.returncode:
        sys.stderr.write(completed_process.stdout.decode())
        exit(completed_process.returncode)
    else:
        sys.stdout.write(completed_process.stdout.decode())
        print('Downloads completed successfully')


if __name__ == '__main__':
    os.makedirs('download', exist_ok=True)
    syn = synapse_login()
    download_tcga_data(syn)
    check_download_checksums()
